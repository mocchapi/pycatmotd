============================================
--------------------------------------------
pycatmotd          v1.0.0           by anne
--------------------------------------------
============================================

pycatmotd is a small utility that
fetches a random image of a cat from
https://thecatapi.com/ & turns it into
ascii art using jp2a

it then puts this ascii art in /etc/motd
so that when you log in through 
terminal you get a cool lil ascii cat

results can vary wildly so probably
run it a few times until youre satisfied

also this will completely overwrite 
/etc/motd so back up stuff you have in
there

ALSO it requires root. if you instelled
elevate itll ask for it automatically


-----------------requirements:--------------
pip3:
- elevate (optional)

apt (or w/e package manager):
- jp2a
--------------------------------------------