import requests
import json
from subprocess import Popen, PIPE
try:
	from elevate import elevate
	elevate()
except:
	pass

def getImage(filename='temp.jpg'):
	url = '.gif'
	while url.endswith('.gif'):
		r = requests.get('https://api.thecatapi.com/v1/images/search?size=full')
		url = json.loads(r.text)[0]['url']
	print(f'got image "{url}"')
	print(f'saving to {filename}')
	with open(filename,'wb') as f:
		f.write(requests.get(url).content)
	print('saved')
	return filename,url

def getAscii(includeUrl=True):
	image,url = getImage()
	commands = ['jp2a',image,'--background=dark','--term-zoom','-b']
	p = Popen(commands, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	output, err = p.communicate(b"input data that is passed to subprocess' stdin")
	# rc = p.returncode
	if includeUrl:
		return f'{output.decode("ascii")}->{url}<-'
	else:
		return output.decode("ascii")


if __name__ == "__main__":
	output = getAscii()
	print(output)
	try:
		with open('/etc/motd','w') as f:
			f.write(output)
		print('motd updated')
	except BaseException as e:
		print('writing to /etc/motd failed:',e)
		print('are we root?')